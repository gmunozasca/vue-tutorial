import { createApp } from 'vue/dist/vue.esm-bundler.js'

const person = {
    name: '',
    lastname: '',
    country: '',
    hobbies: [],
    description: '',
    gender: '',
    age: 0
}

createApp({
    data() {
        return {
            logged: false,
            errorLogin: false,
            editing: true,
            username: '',
            password: '',
            editModel: {
                ...person
            },
            resultModel: {
                ...person
            },
            countries: [
              { text: 'Perú', value: 'PE' },
              { text: 'España', value: 'ES' }
            ]
        }
    },
    methods: {
        logIn() {
            if (this.username == 'admin' && this.password == '123456') {
                this.logged = true
            } else {
                this.errorLogin = true
            }
        },
        hideErrorLogin() {
            this.errorLogin = false
        },
        updateModel() {
            this.editing = true
            this.editModel = Object.assign([], this.resultModel)
        },
        cancelModel() {
            this.editing = false
            this.editModel = Object.assign([], [])
        },
        onSubmit() {
            this.resultModel = Object.assign([], this.editModel)
            this.editModel = Object.assign([], [])
            this.editing = false
        }
    },
    computed: {
        showForm() {
            return this.logged && this.editing
        },
        showCancel() {
            return this.resultModel.name
        },
        fullName() {
            const name = this.editing ? this.editModel.name : this.resultModel.name
            const lastname = this.editing ? this.editModel.lastname : this.resultModel.lastname
            return name + ' ' + lastname
        },
        getCountryName() {
            const country = this.editing ? this.editModel.country : this.resultModel.country
            for (const element of this.countries) {
                if (country === element.value) {
                    return element.text
                }
            }
            return ''
        },
        getHobbies() {
            const hobbies = this.editing ? this.editModel.hobbies : this.resultModel.hobbies
            return hobbies.join(', ')
        },
        getAge() {
            const age = this.editing ? this.editModel.age : this.resultModel.age
            return age > 0 ? age : ''
        },
        getGender() {
            return this.editing ? this.editModel.gender : this.resultModel.gender
        },
        getDescription() {
            return this.editing ? this.editModel.description : this.resultModel.description
        }
    }
}).mount('#app')
